import firebase from 'firebase'

const config = {
  apiKey: "AIzaSyCin2SQrhIMdqPzXJHzg0mV3QNsya2K7Yo",
  authDomain: "noteyandroid.firebaseapp.com",
  databaseURL: "https://noteyandroid.firebaseio.com/",
  projectId: "noteyandroid",
  storageBucket: "gs://noteyandroid.appspot.com",
  messagingSenderId: "1062753276380"
}

firebase.initializeApp(config)

export default firebase
export const db = firebase.database()
export const auth = firebase.auth()
