import Vue from 'vue'
import Vuex from 'vuex'
import LoginPage from './components/LoginPage'
import HomePage from './components/HomePage'
import ProfilePage from './components/ProfilePage'
import firebase from './firebase'
import {auth, db} from './firebase'
import md5 from './components/md5'

Vue.use(Vuex)

export default new Vuex.Store({
  state: { // global state
    appTitle: 'NOTEY',
    user: {
      email: '',
      displayName: ''
    },
    fblogin: false,
    gglogin: false,
    authen: false,
    error: null,
    loading: false,
    loading2: false,
    provId: '',
    menuBarStatus: true,
    pageStack: [],
    toolBarMsgStack: [],
    toolbarMsg: "Welcome"
  },
  mutations: {
    setAuthen(state, payload){
      state.authen = payload
    },
    setUser (state, payload) {
      state.user = payload
    },
    setFbLogin (state, payload) {
      state.fblogin = payload
    },
    setGgLogin (state, payload) {
      state.gglogin = payload
    },
    setError (state, payload) {
      state.error = payload
    },
    setLoading (state, payload) {
      state.loading = payload
    },
    setLoading2 (state, payload) {
      state.loading2 = payload
    },
    setProvId (state, payload) {
      state.provId = payload
    },
    setMenuBarStatus (state, payload) {
      state.menuBarStatus = payload
    },
    setPageStack (state, payload) {
      state.pageStack = payload
    },
    setToolbarMsg (state, payload) {
      state.toolbarMsg = payload
    },
    pushToolbarMsgSetter (state, payload) {
      state.toolBarMsgStack.push(payload)
      state.toolbarMsg = payload
    },
    popToolbarMsgSetter (state) {
      if (state.toolBarMsgStack.length > 1) {
        state.toolBarMsgStack.pop()
      }
      state.toolbarMsg = state.toolBarMsgStack[state.toolBarMsgStack.length -1]
    }
  },
  actions: {
    userSignUp ({commit}, payload) {
      commit('setLoading', true)
      return new Promise((resolve, reject) => {
        auth.createUserWithEmailAndPassword(payload.email, payload.password)
          .then(firebaseUser => {
            commit('setUser', {email: firebaseUser.email, displayName: payload.displayName})
            commit('setLoading', false)
            auth.currentUser.updateProfile({
              displayName: payload.displayName,
            }).catch(function(error) {
              console.error();
            })
            auth.currentUser.sendEmailVerification()
            alert('Please check your email to complete sign up.')
            auth.signOut()
            commit('setAuthen', false)
            resolve(true)
          })
          .catch(error => {
            commit('setError', error.message)
            commit('setLoading', false)
            reject(error.message)
          })
      })
    },
    userSignIn ({commit}, payload) {
      commit('setLoading', true)
      // eslint-disable-next-line
      return new Promise((resolve, reject) => {
        auth.signInWithEmailAndPassword(payload.email, payload.password)
          .then(firebaseUser => {
            commit('setUser', {email: firebaseUser.user.email, displayName: firebaseUser.user.displayName})
            commit('setLoading', false)
            commit('setError', null)
            commit('setAuthen', true)
            resolve(true)
          })
          .catch(error => {
            commit('setError', error.message)
            commit('setLoading', false)
            commit('setGgLogin', false)
            commit('setFbLogin', false)
            reject(error.message)
          })
      })

    },
    autoSignIn ({commit}, payload) {
      if (auth.currentUser.providerData[0].providerId === 'facebook.com') {
        commit('setFbLogin', true)
      } else if (auth.currentUser.providerData[0].providerId === 'google.com') {
        commit('setGgLogin', true)
      }
      commit('setUser', {email: payload.email, displayName: auth.currentUser.displayName})
      commit('setAuthen', true)
    },
    userSignOut ({commit}) {
      return new Promise((resolve, reject) => {
        auth.signOut().then(() => {
          commit('setAuthen', false)
          commit('setGgLogin', false)
          commit('setFbLogin', false)
          // commit('splitter/toggle', false)
          resolve(true)
        }).catch((error) => {
          reject(error.message)
        })
      })
    },
    changeDisplayName ({commit}, payload) {
      auth.currentUser.updateProfile({
        displayName: payload.newDisplayName
      }).catch(function(error) {
        console.error();
      })
      commit('setUser', {displayName: payload.newDisplayName, email: auth.currentUser.email})
    },
    changePassword ({commit}, payload) {
      const user = auth.currentUser
      const cred = firebase.auth.EmailAuthProvider.credential(
        user.email, payload.curPassword)
      if (user.reauthenticateWithCredential(cred)) {
        user.updatePassword(payload.newPassword).then(() => {
          alert("Password updated!")
        }).catch((error) => { console.log(error) })
      }
    },
    changeEmail ({commit}, payload) {
      const user = auth.currentUser
      const cred = firebase.auth.EmailAuthProvider.credential(
        user.email, payload.curPassword)
      if (user.reauthenticateWithCredential(cred)) {
        user.updateEmail(payload.newEmail).then(() => {
          alert("Email updated!")
          commit('setUser', {displayName: user.displayName, email: payload.newEmail})
        }).catch((error) => { console.log(error) })
      }
    },
    resetPassword ({commit}, payload) {
      auth.sendPasswordResetEmail(payload.email).then(function() {
        alert('Please check your email.')
      }).catch(function(error) {
        alert(error)
      });
    },
    uploadImage ({commit}, file) {
      return new Promise((resolve, reject) => {
        const baseURL = 'images/'
        const storageRef = firebase.storage().ref()
        const hashName = md5(file.name)
        const thisRef = storageRef.child(baseURL + hashName)
        thisRef.put(file).then(function(snapshot) {
          resolve(snapshot)
        }).catch(function (error) {
          switch (error.code) {
            case 'storage/object_not_found':
              // File doesn't exist
              reject('File doesn\'t exist')
              break

            case 'storage/unauthorized':
              // User doesn't have permission to access the object
              reject('Permission denied')
              break

            case 'storage/canceled':
              // User canceled the upload
              reject('Upload canceled')
              break

            case 'storage/unknown':
              // Unknown error occurred, inspect the server response
              reject('Unknown error occurred')
              break
          }
        })
      })
    },
    /*
      To call this function:
        store.dispatch('getImage',snapshot.metadata.fullPath).then(function(url) {
          console.log(url)
        }).catch(function(error) {
          console.log(error)
        })
     */
    getImage ({commit}, imgUrl) {
      return new Promise((resolve, reject) => {
        const baseURL = 'images/'
        const storageRef = firebase.storage().ref()
        // Create a reference to the file we want to download
        var thisRef = storageRef.child(imgUrl)
        thisRef.getDownloadURL().then(function(url) {
          resolve(url)
        }).catch(function(error) {
          switch (error.code) {
            case 'storage/object_not_found':
              // File doesn't exist
              reject('File doesn\'t exist')
              break

            case 'storage/unauthorized':
              // User doesn't have permission to access the object
              reject('Permission denied')
              break

            case 'storage/canceled':
              // User canceled the upload
              reject('Upload canceled')
              break

            case 'storage/unknown':
              // Unknown error occurred, inspect the server response
              reject('Unknown error occurred')
              break
          }
        })
      })
    },
    pushToDb ({commit}, path, valObj) {
      db.ref(path).push(valObj)
    },
    changeToolbarMsg({commit}, msg) {
      commit('setToolbarMsg', msg)
    },
    pushToolbarMsg({commit}, payload) {
      commit('pushToolbarMsgSetter', payload)
    },
    popToolbarMsg({commit}) {
      commit('popToolbarMsgSetter')
    }
  },
  getters: {
    isAuthenticated (state) {
      return state.authen
    },
    getToolbarMsg (state) {
      return state.toolbarMsg;
    }
  },
  modules: {
    splitter: {
      namespaced: true,
      state: {
        open: false,
        pageStack: []
      },
      mutations: {
        toggle (state, shouldOpen) {
          console.log('toggle func')
          if (typeof shouldOpen === 'boolean') {
            console.log('if')
            state.open = shouldOpen
          } else {
            console.log('else')
            state.open = !state.open
          }
        },
        pushPage(state, page) {
          state.pageStack = [
            ...state.pageStack,
            ...(page instanceof Array ? page : [page])
          ];
        },
        popPage(state) {
          if (state.pageStack.length > 1) {
            state.pageStack.pop();
          }
        },
        replacePage(state, page) {
          state.pageStack.pop();
          state.pageStack.push(page);
        },
        resetPageStack(state) {
          state.pageStack = [state.pageStack[0]];
        }
      }
    }
  }
})
